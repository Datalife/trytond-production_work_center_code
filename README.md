datalife_production_work_center_code
====================================

The production_work_center_code module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_work_center_code/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_work_center_code)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
